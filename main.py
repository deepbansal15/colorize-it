import torch
from torch import optim
from torch.utils.data import DataLoader

from tqdm import tqdm

from color_estimator import ColorEstimator
from coco_dataset import COCODataset

BATCH_SIZE = 30

# TODO Loss, Metrics, Model Checkpoint, Multi-GPU 
# 

loss = torch.nn.MSELoss()

def loss_function(y, gt):
    return loss(y, gt)

def main(epoch_count, log_interval, save_interval, use_cuda):
    # data = torch.ones([16,1,128,128])

    # output = net(da ta)
    # print(output.shape)

    device = torch.device("cuda" if use_cuda else "cpu")

    dataset = COCODataset('COCO')
    num_batches = int(len(dataset) / BATCH_SIZE)
    dataloader = DataLoader(dataset, batch_size=BATCH_SIZE, shuffle=True, num_workers=8, drop_last=True)
    
    print(len(dataloader), len(dataloader.dataset))
    in_out_channels = [1,2,4,8,16,16,8,4,2,1]
    block_scales = [None, 'down', None, 'down', None, 'down', None, 'down', None, 'down']
    
    model = ColorEstimator(96, in_out_channels, block_scales,device)
    model = torch.nn.DataParallel(model)
    model = model.to(device)
    optimizer = optim.Adam(model.parameters(), lr=1e-3)
    
    model.train()
    train_loss = 0

    for epoch in range(1, epoch_count + 1):
        for batch_idx, batch in tqdm(enumerate(dataloader), total=num_batches):
            image = batch['image']
            cluster = batch['cluster']
            
            x = image.to(device)
            gt = cluster.to(device)
            
            optimizer.zero_grad()

            y = model(x)
            loss = loss_function(y, gt)
            loss.backward()

            train_loss += loss.item()
            optimizer.step()
            
            if batch_idx % log_interval == 0:
                # Log here
                print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                    epoch, batch_idx, len(dataloader),
                    100. * batch_idx / len(dataloader),
                    loss.item()))
        
        print('====> Epoch: {} Average loss: {:.4f}'.format(
          epoch, train_loss / num_batches))
        
        if epoch % save_interval == 0:
            # Save here
            torch.save(model.state_dict(), 'checkpoint/color-estimator')
            pass

if __name__ == '__main__':
    main(50, 500, 5, True)