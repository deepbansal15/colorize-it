import os
import argparse
import time
import glob

from skimage import color, io

import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from sklearn.metrics import pairwise_distances_argmin
from sklearn.datasets import load_sample_image
from sklearn.utils import shuffle

from scipy.cluster.vq import kmeans2
from time import time

from pathlib import Path

NCLUSTERS = 16

def hasCluster(path_clusters, filename):
    cluster_path = path_clusters + '/' + filename + '.npy'
    if Path(cluster_path).is_file():
        return True
    else:
        return False

def main(basepath):
    # Get all the files
    path = os.path.join(basepath, 'images', '*.jpg')
    files = glob.glob(path)
    files = shuffle(files)
    img_num = len(files)

    print(img_num)

    path_clusters = os.path.join(basepath, 'clusters')
    for img_path in files:
        t0 = time()
        filename = os.path.splitext(os.path.basename(img_path))[0]

        if hasCluster(path_clusters, filename):
            img_num -= 1
            continue

        img_rgb = io.imread(img_path)
        if len(img_rgb.shape) != 3:
            continue

        if img_rgb.shape[2] != 3:
            continue

        w, h, d = original_shape = tuple(img_rgb.shape)
        img_rgb = np.array(img_rgb, dtype=np.float32) / 255.
        image_array = np.reshape(img_rgb, (w * h, d))
        image_array_sample = shuffle(image_array, random_state = 0)
        kmeans = KMeans(n_clusters=NCLUSTERS, init='k-means++', random_state = 0, n_jobs = -1, precompute_distances=True).fit(image_array_sample)
        # centroid, _ = kmeans2(image_array_sample, NCLUSTERS)
        cluster_img_w = np.sqrt(NCLUSTERS).astype(int)
        centres = np.reshape(kmeans.cluster_centers_.copy(), (cluster_img_w , cluster_img_w, d))
        # centres = np.reshape(centroid.copy(), (cluster_img_w , cluster_img_w, d))
        np.save(path_clusters + '/' + filename, centres, allow_pickle=False)
        print("done in %0.3fs." % (time() - t0))
        img_num -= 1
        print('Images left {}'.format(img_num))

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-path', type=str, help='Path to dataset.')
    args = parser.parse_args()
    main(args.path)