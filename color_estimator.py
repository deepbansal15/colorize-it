# Estimates the K primary color of an image from its B/W image
# dim(K) = (4,4,3) i.e., 16 primary colors

import torch
import torch.nn as nn
import torch.nn.functional as F

class SelfAttention(nn.Module):
    def __init__(self, in_channel, k=1):
        super().__init__()

        self.query = nn.Conv2d(in_channel, in_channel // k, 1)
        self.key = nn.Conv2d(in_channel, in_channel // k, 1)
        self.value = nn.Conv2d(in_channel, in_channel, 1)

        self.gamma = nn.Parameter(torch.tensor(0.0))

    def forward(self, input):
        shape = input.shape
        # flatten = input.view(shape[0], shape[1], -1)
        query = self.query(input)
        query = query.view(shape[0], query.shape[1], -1).permute(0, 2, 1)
        
        key = self.key(input)
        key = key.view(shape[0], key.shape[1], -1)
        
        value = self.value(input)
        value = value.view(shape[0], value.shape[1], -1)

        query_key = torch.bmm(query, key)
        attn = F.softmax(query_key, 1)
        attn = torch.bmm(value, attn)
        attn = attn.view(*shape)
        out = self.gamma * attn + input

        return out

class ResnetBlock(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size=(3,3), scale=None, use_projection=False):
        super().__init__()
        self.scale = scale
        self.conv1 = nn.Conv2d(in_channels, out_channels, kernel_size, padding=1)
        self.relu = nn.ReLU()
        self.conv2 = nn.Conv2d(out_channels, out_channels, kernel_size, padding=1)

        if scale == 'down':
            self.pool = nn.AvgPool2d((2,2), stride=2)

        # if use_projection or scale != None:
        self.shorcut_conv = nn.Conv2d(in_channels=in_channels, out_channels=out_channels, kernel_size=(1,1))
    
    def forward(self, x):
        y = self.relu(x)
        y = self.conv1(y)
        y = self.relu(y)
        y = self.conv2(y)

        shorcut = self.shorcut_conv(x)

        if self.scale != None:
            y = self.pool(y)
            shorcut = self.pool(shorcut)
        return y + shorcut
        


class ColorEstimator(nn.Module):
    def __init__(self, ch_multiplier, in_out_channels, block_scales,device):
        super().__init__()
        self.block_count = len(in_out_channels)
        in_channels = [1] + [ch_multiplier * c for c in in_out_channels[:-1]]
        out_channels = [ch_multiplier * c for c in in_out_channels]
        blocks = []
        for i in range(self.block_count):
            blocks.append(ResnetBlock(in_channels[i], out_channels[i], scale=block_scales[i]))
        
        self.block_layer = nn.Sequential(*blocks)
        
        self.conv = nn.Conv2d(out_channels[-1], 3, kernel_size=[3,3], padding=1)
        self.relu = nn.ReLU()
        self.tanh = nn.Tanh()

    def forward(self, x):
        x = self.block_layer(x)
        
        x = self.relu(x)
        x = self.conv(x)
        return (self.tanh(x) + 1.0) / 2.0
