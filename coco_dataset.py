import os
import glob

import numpy as np
from skimage import io, color, transform


import torch
from torch.utils.data import Dataset, DataLoader
from torchvision import utils

IMAGE_SIZE = 128
CROP_PADDING = 16

def centre_crop_and_resize(img_rgb, image_size=IMAGE_SIZE):
    h,w,c = original_shape = img_rgb.shape
    padded_center_crop_size = int((image_size / (image_size + CROP_PADDING)) * float(min(h, w)))
    
    # Centre crop
    top = np.random.randint(0, h - padded_center_crop_size)
    left = np.random.randint(0, w - padded_center_crop_size)

    img_rgb = img_rgb[top: top + padded_center_crop_size,
                      left: left + padded_center_crop_size]

    # Resize using skimage transform
    img_rgb = transform.resize(img_rgb, (image_size,image_size))
    return img_rgb


class COCODataset(Dataset):
    def __init__(self, path):
        cluster_path = os.path.join(path, 'clusters', '*.npy')
        self.cluster_files = glob.glob(cluster_path)

        # No need to shuffle the clusters datasetloader do the shuffling

        self.img_files = [path.replace('clusters','images').replace('npy','jpg') for path in self.cluster_files] 
        self.img_num = len(self.img_files)

    def __len__(self):
        return self.img_num

    def __getitem__(self, idx):
        img_rgb = io.imread(self.img_files[idx])
        img_rgb = centre_crop_and_resize(img_rgb)

        # img_rgb = img_rgb.astype('float32')
        # img_gray = img_rgb[:,:,0] + img_rgb[:,:,1] + img_rgb[:,:,2]
        # img_gray = img_gray / (3. * 255.)

        # rgb2gray gives a float64 array and removes the channel dimension
        img_gray = color.rgb2grey(img_rgb)
        img_gray = img_gray.astype('float32')
        # [H,W] -> [H,W,C] -> [C,H,W]
        img_gray = np.reshape(img_gray, (img_gray.shape[0], img_gray.shape[1], 1))
        image = img_gray.transpose((2, 0, 1))
        img_tensor = torch.from_numpy(image)
        
        cluster = np.load(self.cluster_files[idx])
        cluster = cluster.transpose((2, 0, 1))
        cluster_tensor = torch.from_numpy(cluster)

        return {'image':img_tensor, 'cluster':cluster_tensor}
